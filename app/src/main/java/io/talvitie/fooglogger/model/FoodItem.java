package io.talvitie.fooglogger.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by will on 9.4.2018.
 */


@Entity
public class FoodItem {
    public FoodItem(long id, String foodItemName, Boolean suitable, Boolean allergenic, String symptoms, Integer timesEaten) {
        this.id = id;
        this.foodItemName = foodItemName;
        this.suitable = suitable;
        this.allergenic = allergenic;
        this.symptoms = symptoms;
        this.timesEaten = timesEaten;
    }

    @PrimaryKey
    public long id;
    public String foodItemName;

    public Boolean suitable;
    public Boolean allergenic;
    public String symptoms;
    public Integer timesEaten;

    public String getFoodItemName() {
        return foodItemName;
    }

    public Boolean getSuitable() {
        return suitable;
    }

    public Boolean getAllergenic() {
        return allergenic;
    }

    public String getSymptoms() {
        return symptoms;
    }
}
