package io.talvitie.fooglogger.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by will on 16.4.2018.
 */

@Entity
public class FoodFoodItem {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public long foodId;
    public long foodItemId;

    public FoodFoodItem(long id, long foodId, long foodItemId) {
        this.id = id;
        this.foodId = foodId;
        this.foodItemId = foodItemId;
    }
}
