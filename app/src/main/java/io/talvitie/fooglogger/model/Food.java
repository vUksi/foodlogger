package io.talvitie.fooglogger.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

import io.talvitie.fooglogger.db.DateConverter;

/**
 * Created by will on 9.4.2018.
 */
@Entity
public class Food {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String name;
    @TypeConverters(DateConverter.class)
    private Date dateEaten;
    private Boolean allergenic;
    private String symptoms;

    /**
     * Constructor for food
     * @param name Name of food eaten
     * @param dateEaten Date when food was eaten
     * @param allergenic Did the food cause allergies, if false food is considered safe
     * @param symptoms Possible symptoms if allergenic
     */
    public Food(String name, Date dateEaten, Boolean allergenic, String symptoms) {
        this.name = name;
        this.dateEaten = dateEaten;
        this.allergenic = allergenic;
        this.symptoms = symptoms;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getDateEaten() {
        return dateEaten;
    }

    public Boolean getAllergenic() {
        return allergenic;
    }


    public String getSymptoms() {
        return symptoms;
    }


    public void setDateEaten(Date dateEaten) {
        this.dateEaten = dateEaten;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setAllergenic(Boolean allergenic) {
        this.allergenic = allergenic;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }



}
