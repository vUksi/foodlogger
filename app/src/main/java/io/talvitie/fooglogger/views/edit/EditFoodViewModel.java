package io.talvitie.fooglogger.views.edit;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.concurrent.ExecutionException;

import io.talvitie.fooglogger.db.FoodDatabase;
import io.talvitie.fooglogger.model.Food;

public class EditFoodViewModel extends AndroidViewModel {

    private FoodDatabase db;

    public EditFoodViewModel(@NonNull Application application) {
        super(application);

        db = FoodDatabase.getDatabase(this.getApplication());
    }

    public Food getFood(int id) throws ExecutionException, InterruptedException {

        return  new getAsyncTask(db).execute(id).get();
    }


    public void updateFood(Food food) {new editAsyncTask(db).execute(food);}

    private static class editAsyncTask extends AsyncTask<Food,Void,Void> {

        private FoodDatabase db;

        editAsyncTask(FoodDatabase database) {db = database;}

        @Override
        protected Void doInBackground(final Food... params) {
            db.foodModel().updateFood(params[0]);
            return null;
        }
    }

    private static class getAsyncTask extends AsyncTask<Integer,Void,Food> {

        private FoodDatabase db;

        getAsyncTask(FoodDatabase database) {db = database;}

        @Override
        protected Food doInBackground(final Integer... params) {
            Food food = db.foodModel().getFoodById(params[0]);
            return food;
        }
    }

    public void deleteFood(Food food) {
        new deleteAsyncTask(db).execute(food);
    }

    private static class deleteAsyncTask extends AsyncTask<Food, Void, Void> {

        private FoodDatabase db;

        deleteAsyncTask(FoodDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Food... params) {
            db.foodModel().deleteFood(params[0]);
            return null;
        }

    }
}
