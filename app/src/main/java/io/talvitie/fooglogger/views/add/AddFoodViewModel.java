package io.talvitie.fooglogger.views.add;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;


import io.talvitie.fooglogger.db.FoodDatabase;
import io.talvitie.fooglogger.model.Food;

public class AddFoodViewModel extends AndroidViewModel {

    private FoodDatabase db;

    public AddFoodViewModel(@NonNull Application application) {
        super(application);

        db = FoodDatabase.getDatabase(this.getApplication());

    }



    public void addFood(Food food) {
        new addAsyncTask(db).execute(food);
    }

    private static class addAsyncTask extends AsyncTask<Food, Void, Void> {

        private FoodDatabase db;

        addAsyncTask(FoodDatabase database) {
            db = database;
        }

        @Override
        protected Void doInBackground(final Food... params) {
            db.foodModel().addFood(params[0]);
            return null;
        }

    }
}
