package io.talvitie.fooglogger.views.add;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import io.talvitie.fooglogger.R;
import io.talvitie.fooglogger.model.Food;
import io.talvitie.fooglogger.views.edit.EditFoodActivity;

public class AddFoodActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private Date date;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;

    private EditText dateEatenText;
    private EditText foodNameEditText;
    private Switch switchAllergic;
    private EditText symptoms;

    private AddFoodViewModel addViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        foodNameEditText = findViewById(R.id.foodName);
        dateEatenText = findViewById(R.id.dateEaten);
        switchAllergic =  findViewById(R.id.allergic);
        symptoms = findViewById(R.id.symptoms);

        calendar = Calendar.getInstance();
        date = new Date();
        dateEatenText.setText(calendar.getTime().toString());
        addViewModel = ViewModelProviders.of(this).get(AddFoodViewModel.class);

        datePickerDialog = new DatePickerDialog(this, AddFoodActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        switchAllergic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    symptoms.setEnabled(true);
                } else {
                    symptoms.setEnabled(false);
                }

            }
        });


        findViewById(R.id.datePicker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });



    }

    /**
     * SHow only the confirm button for menu, no need for delete
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_confirm, menu);
        MenuItem item = menu.findItem(R.id.miDelete);
        item.setVisible(false);
        return true;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        date = calendar.getTime();
        dateEatenText.setText(date.toString());

    }

    public void onDeleteAction(MenuItem mi) {
        finish();
    }
    public void onCancelAction(MenuItem mi) {
        finish();
    }
    public void onConfirmAction(MenuItem mi) {
        if ( foodNameEditText.getText() == null || date == null)
            Toast.makeText(AddFoodActivity.this, "Missing fields", Toast.LENGTH_SHORT).show();
        else {

            String symptomsTmp =symptoms.getText().toString();

            if(!symptoms.isEnabled()) {
                symptomsTmp = "";
            }

            addViewModel.addFood(new Food(
                    foodNameEditText.getText().toString(),
                    date,
                    switchAllergic.isChecked(),
                    symptomsTmp
            ));
            finish();
        }
    }




}
