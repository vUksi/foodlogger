package io.talvitie.fooglogger.views.edit;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import io.talvitie.fooglogger.R;
import io.talvitie.fooglogger.model.Food;
import io.talvitie.fooglogger.views.add.AddFoodActivity;
import io.talvitie.fooglogger.views.add.AddFoodViewModel;

public class EditFoodActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private Date date;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;

    private EditText dateEatenText;
    private EditText foodNameEditText;
    private Switch switchAllergic;
    private EditText symptoms;

    private Food editableFood;
    private EditFoodViewModel editFoodViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_food);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        foodNameEditText = findViewById(R.id.foodName);
        dateEatenText = findViewById(R.id.dateEaten);
        switchAllergic =  findViewById(R.id.allergic);
        symptoms = findViewById(R.id.symptoms);

        editFoodViewModel = ViewModelProviders.of(this).get(EditFoodViewModel.class);
        switchAllergic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    symptoms.setEnabled(true);
                } else {
                    symptoms.setEnabled(false);
                }

            }
        });
        calendar = Calendar.getInstance();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            try {
                getAndSetFood(extras.getInt("foodId"));
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        datePickerDialog = new DatePickerDialog(this, EditFoodActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        findViewById(R.id.datePicker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
    }

    /**
     * Fetch food from database by id passed from main activity.
     * Updates view fields with values from food entity.
     * @param id
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private void getAndSetFood(int id) throws ExecutionException, InterruptedException {
        editableFood = editFoodViewModel.getFood(id);
        if (editableFood != null) {
            dateEatenText.setText(editableFood.getDateEaten().toString());
            date =editableFood.getDateEaten();
            foodNameEditText.setText(editableFood.getName());
            symptoms.setText(editableFood.getSymptoms());
            switchAllergic.setChecked(editableFood.getAllergenic());


        }
    }

    /**
     * Update entity for saving to database.
     * If food is set as not allergic clear possible symptoms before saving.
     */
    private void updateFood() {
        editableFood.setName(foodNameEditText.getText().toString());
        editableFood.setDateEaten(date);
        editableFood.setAllergenic(switchAllergic.isChecked());
        String symptomsTmp =symptoms.getText().toString();

        if(!symptoms.isEnabled()) {
            symptomsTmp = "";
        }
        editableFood.setSymptoms(symptomsTmp);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        date = calendar.getTime();
        dateEatenText.setText(date.toString());
    }
    public void onDeleteAction(MenuItem mi) {
        editFoodViewModel.deleteFood(editableFood);
        finish();
    }

    public void onConfirmAction(MenuItem mi) {
        if ( foodNameEditText.getText() == null || date == null)
            Toast.makeText(EditFoodActivity.this, "Missing fields", Toast.LENGTH_SHORT).show();
        else {
            updateFood();
            editFoodViewModel.updateFood(editableFood);
            finish();
    }}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_confirm, menu);
        return true;
    }
    public void onCancelAction(MenuItem mi) {
        finish();
    }


}

