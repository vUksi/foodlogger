package io.talvitie.fooglogger.views.list;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import io.talvitie.fooglogger.db.FoodDatabase;

import android.support.annotation.NonNull;

import java.util.List;

import io.talvitie.fooglogger.model.Food;
import io.talvitie.fooglogger.util.ListType;

public class FoodListViewModel extends AndroidViewModel {

    private final LiveData<List<Food>> foodList;


    private FoodDatabase db;

    public FoodListViewModel(@NonNull Application application) {
        super(application);

    db = FoodDatabase.getDatabase(this.getApplication());
    foodList = db.foodModel().getAllFoods();

    }


    public LiveData<List<Food>> getFoodList() {

            return foodList;


    }
}
