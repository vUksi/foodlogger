package io.talvitie.fooglogger.views;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import io.talvitie.fooglogger.R;
import io.talvitie.fooglogger.RecyclerViewAdapter;
import io.talvitie.fooglogger.model.Food;
import io.talvitie.fooglogger.util.ListType;
import io.talvitie.fooglogger.views.add.AddFoodActivity;
import io.talvitie.fooglogger.views.edit.EditFoodActivity;
import io.talvitie.fooglogger.views.list.FoodListViewModel;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private FoodListViewModel viewModel;
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView =  findViewById(R.id.recyclerView);
        recyclerViewAdapter = new RecyclerViewAdapter(new ArrayList<Food>(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = findViewById(R.id.addFood);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddFoodActivity.class);
                startActivity(intent);


            }
        });


        recyclerView.setAdapter(recyclerViewAdapter);

        viewModel = ViewModelProviders.of(this).get(FoodListViewModel.class);

        viewModel.getFoodList().observe(MainActivity.this, new Observer<List<Food>>() {
            @Override
            public void onChanged(@Nullable List<Food> foodList) {
                recyclerViewAdapter.addItems(foodList);
            }
        });

    }

    /**
     * On click for recyclerview items, takes user to clicked food
     * @param v
     */
    @Override
    public void onClick(View v) {
        Food food = (Food) v.getTag();
        Intent intent = new Intent(MainActivity.this, EditFoodActivity.class);
        intent.putExtra("foodId",food.id);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_help, menu);
        return true;
    }


    public void onHelpAction(MenuItem mi) {
        Intent intent = new Intent(MainActivity.this, HelpActivity.class);
        startActivity(intent);
    }

}
