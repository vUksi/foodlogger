package io.talvitie.fooglogger;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.talvitie.fooglogger.model.Food;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private List<Food> foodList;
    private View.OnClickListener clickListener;

    public RecyclerViewAdapter(List<Food> foodList, View.OnClickListener clickListener) {
        this.foodList = foodList;
        this.clickListener = clickListener;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        Food food = foodList.get(position);
        holder.nameTextView.setText(food.getName());
        holder.dateTextView.setText(food.getDateEaten().toString().substring(0, 11));
        holder.symptomsTextView.setText(food.getSymptoms().toString());
        holder.itemView.setTag(food);
        holder.itemView.setOnClickListener(clickListener);
        if (food.getAllergenic()) {
            holder.okFace.setVisibility(View.INVISIBLE);
            holder.allergyFace.setVisibility(View.VISIBLE);
        }
        else {
            holder.okFace.setVisibility(View.VISIBLE);
            holder.allergyFace.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public void addItems(List<Food> foodList) {
        this.foodList = foodList;
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView dateTextView;
        private TextView symptomsTextView;
        private ImageView okFace;
        private ImageView allergyFace;

        RecyclerViewHolder(View view) {
            super(view);
            nameTextView = view.findViewById(R.id.nameTextView);
            dateTextView = view.findViewById(R.id.dateTextView);
            symptomsTextView = view.findViewById(R.id.symptomsTextView);
            okFace = view.findViewById(R.id.suitableFace);
            allergyFace = view.findViewById(R.id.allergyFace);
        }
    }
}