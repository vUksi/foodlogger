package io.talvitie.fooglogger.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.List;

import io.talvitie.fooglogger.db.DateConverter;
import io.talvitie.fooglogger.model.Food;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
@TypeConverters(DateConverter.class)
public interface FoodDao {
    @Query("SELECT * FROM food")
    LiveData<List<Food>> getAllFoods();

    @Query("SELECT * FROM food WHERE allergenic = 0")
    LiveData<List<Food>> getAllSuitableFoods();

    @Query("SELECT * FROM food WHERE allergenic = 1")
    LiveData<List<Food>> getAllAllergenicFoods();

    @Query("SELECT * FROM food WHERE id=:id")
    Food getFoodById(int id);

    @Insert(onConflict = REPLACE)
    void addFood(Food food);

    @Delete
    void deleteFood(Food food);

    @Update
    void updateFood(Food food);
}
