package io.talvitie.fooglogger.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.talvitie.fooglogger.model.FoodItem;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface FoodItemDao {

    @Query("SELECT * from foodItem")
    LiveData<List<FoodItem>> getAllFoodItems();

    @Query("SELECT * FROM FoodItem WHERE id = :id")
    FoodItem getFoodItemById(long id);

    @Insert(onConflict = REPLACE)
    void addFood(FoodItem foodItem);

    @Delete
    void deleteFood(FoodItem foodItem);

}
