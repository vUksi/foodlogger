package io.talvitie.fooglogger.db;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import io.talvitie.fooglogger.db.dao.FoodDao;
import io.talvitie.fooglogger.db.dao.FoodItemDao;
import io.talvitie.fooglogger.model.Food;
import io.talvitie.fooglogger.model.FoodItem;

@android.arch.persistence.room.Database(entities = {Food.class, FoodItem.class},version = 1)
public abstract class FoodDatabase extends RoomDatabase {
    private static FoodDatabase INSTANCE;

    public static FoodDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), FoodDatabase.class, "foodDb")
                            .build();
        }
        return INSTANCE;
    }

    public abstract FoodDao foodModel();
}
